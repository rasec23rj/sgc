using SGC.ApplicationCore.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace SGC.Teste.Data {
    public class ClienteContext : DbContext {
        public ClienteContext (DbContextOptions<ClienteContext> options) : base (options) { }
        public DbSet<Cliente> Clientes { get; set; }

        protected override void OnModelCreating (ModelBuilder modelBuilder) {
           modelBuilder.Entity<Cliente>().ToTable("Tb_Cliente");
        }

    }
}